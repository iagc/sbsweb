﻿using System.Web;
using System.Web.Optimization;

namespace sbs
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            /*
             <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet"> 
    <link href="css/lightbox.css" rel="stylesheet"> 
	<link href="css/main.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">

            <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/lightbox.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/main.js"></script>   
             */

            bundles.Add(new StyleBundle("~/Content/style").Include(
                "~/Content/css1/bootstrap.min.css",
                "~/Content/css1/font-awesome.min.css",
                "~/Content/css1/animate.min.css",
                "~/Content/css1/lightbox",
                "~/Content/css1/main.css",
                "~/Content/css1/responsive.css"
                ));

            bundles.Add(new ScriptBundle("~/Content/js").Include(
                      "~/Content/js/jquery.js",
                      "~/Content/js/bootstrap.min.js",
                      "~/Content/js/gmaps.js",
                      "~/Content/js/jquery.fitvids.js",
                      "~/Content/js/holder.js",
                      "~/Content/js/lightbox.min.js",
                      "~/Content/js/wow.min.js",
                      "~/Content/js/main.js"
                      ));
        }
    }
}
